package com.example.vdl.alarm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

/**
 * Created by vdl on 5/25/16.
 */
public class AlarmReceiver extends BroadcastReceiver {
    public static String ACTION_ALARM = "com.example.vdl.alarm";
    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();
        String action = bundle.getString(ACTION_ALARM);
        if (action.equals(ACTION_ALARM)) {
            Intent inService = new Intent(context,PingService.class);
            context.startService(inService);
        }

    }
}