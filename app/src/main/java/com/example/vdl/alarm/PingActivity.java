package com.example.vdl.alarm;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class PingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            AlarmManager alarms = (AlarmManager) this
                    .getSystemService(Context.ALARM_SERVICE);

            Intent intent = new Intent(getApplicationContext(),
                    AlarmReceiver.class);
            intent.putExtra(AlarmReceiver.ACTION_ALARM,
                    AlarmReceiver.ACTION_ALARM);

            final PendingIntent pIntent = PendingIntent.getBroadcast(this,
                    1234567, intent, PendingIntent.FLAG_UPDATE_CURRENT);

            alarms.setRepeating(AlarmManager.RTC_WAKEUP,
                    System.currentTimeMillis(), 300000, pIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }
        setContentView(R.layout.activity_main);
    }
}
